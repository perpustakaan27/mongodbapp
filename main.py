from models import customers

def showUsers():
    for doc in customers.objects:
        print (doc.username, doc.fullname, doc.email)

def showUsersById():
    for doc in customers.objects(username='Fernanda'):
        print (doc.username, doc.fullname, doc.email)

def insertUser():
    data = {
    "username" : 'Mary',
    "fullname" : 'Mary Marijoa', 
    "email" : 'mary@gmail.com'
    }

    customers(**data).save()

    for doc in customers.objects:
        print (doc.username, doc.fullname, doc.email)

def updateUsersById():
    doc_1 = customers.objects(username='Mary').first()
    print (doc_1.to_json())

    doc_1.username = 'MM'
    doc_1.save()

    print (customers.objects().to_json())

def deleteUsersById():
    doc_1 = customers.objects(username='MM').first()
    doc_1.delete()

    print (customers.objects().to_json())

if __name__ == '__main__':
    mysqldb = customers()
    showUsers()
    showUsersById()
    #updateUsersById()
    #insertUser()
    #deleteUsersById()
